import './App.css';
import Header from './Components/Header';


import React, { Component } from 'react'

export class App extends Component {

  render() {
    return (
      <div className='App'>
        <Header />
      </div>
    )
  }
}

export default App