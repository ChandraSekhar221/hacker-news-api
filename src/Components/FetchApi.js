
export const baseUrl = 'https://hacker-news.firebaseio.com/v0/'

export const newStoriesUrl = `${baseUrl}newstories.json`

export const storyUrl = `${baseUrl}item/`

export const getStory = async (storyId) => {
    const story = await fetch(`${storyUrl + storyId}.json`).then(data => data.json())
    return story
}
export const getStories = async () => {
    const storyIds = await fetch(newStoriesUrl).then(data => data.json())
    const storiesArray = await Promise.all(storyIds.map(getStory))
    return storiesArray
}



