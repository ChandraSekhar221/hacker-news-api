import { getStories } from './FetchApi'
import '../App.css';
import "../Styles/Story.css"
import React, { Component } from 'react'

export class StoriesContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            storeis: [],
            isLoaded: false,
            pageNumber: 0,
            postsPerPage: 10,
        }
    }

    componentDidMount() {
        getStories()
            .then(data => {
                this.setState({
                    storeis: data,
                    isLoaded: true,
                })
            })
    }

    render() {
        let userInput = this.props.inputVal
        let { isLoaded, storeis } = this.state
        if (!isLoaded) {
            return <p>Loading....</p>
        }
        else {
            return (
                storeis.filter(
                    (story) => {
                        return story.title.toLowerCase().includes(userInput.trim().toLowerCase()) || userInput.trim() === ''
                    }
                ).map(each => {
                    return (
                        <div>
                            <article className='story-container'>
                                <div className='story-container-title'>
                                    <h4 className='title'>{each.title} </h4>
                                    <a href={each.url} target='_blank' rel="noreferrer" className='url'>({each.url})</a>
                                </div>
                                <div className='story-container-author'>
                                    <p className='points-by'>Points : <span className='bold'>{each.score}</span> | Author : <span className='bold'>{each.by}</span></p>
                                </div>
                            </article>

                        </div>
                    )
                })
            )
        }
    }
}

export default StoriesContainer