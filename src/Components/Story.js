import { getStory } from './FetchApi'
import React, { Component } from 'react'
import StoryStyles from "../Styles/Story.css"

class Story extends Component {

    constructor(props) {
        super(props)

        this.state = {
            story: {},
        }
    }

    componentDidMount() {
        getStory(this.props.storyId)
            .then(data => {
                this.setState({
                    isLoad: true,
                    story: data,
                })
            })
    }

    render() {
        let { story } = this.state
        let userINPUT = this.props.userEnteredValue
        if (!this.props.isLoaded) {
            return <p>Loading ...</p>
        }
        else {
            if (userINPUT === '') {
                return (
                    <article className='story-container'>
                        <p className='title-url title'>{story.title} <a href='#' target='_blank' className='url'>({story.url})</a></p>
                        <p className='points-by'><span>{story.score}</span> points | <span>{story.by}</span></p>
                    </article>

                )
            } else if (story.title.toLowerCase().includes(userINPUT.toLowerCase())) {
                return (
                    <article className='story-container'>
                        <p className='title-url title'>{story.title} <a href='#' target='_blank' className='url'>({story.url})</a></p>
                        <p className='points-by'><span>{story.score}</span> points | <span>{story.by}</span></p>
                    </article>

                )
            } else {
                return null
            }
        }
    }
}

export default Story




