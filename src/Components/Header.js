import '../Styles/Header.css'
import * as Icon from 'react-bootstrap-icons'
import { StoriesContainer } from './StoriesContainer'


import React, { Component } from 'react'

class Header extends Component {

    constructor(props) {
        super(props)

        this.state = {
            inputValue: ''
        }
    }

    inputChange = (event) => {
        this.setState(prevState => ({
            inputValue: event.target.value,
        }))
    }

    render() {
        const { inputValue } = this.state
        return (
            <main>
                <header>
                    <div className='logo-container'>
                        <img className='h-image' alt='illa' src='https://hn.algolia.com/packs/media/images/logo-hn-search-a822432b.png'></img>
                        <p className='logo-text'><span>Search</span><br /><span>Hacker News</span></p>
                    </div>
                    <div className='search-container'>
                        <input type='search' placeholder='Search stories by title, url or author' className='search-input' onChange={this.inputChange}></input>
                    </div>
                    <div className='setting-container'>
                        <Icon.Gear className='gear-icon' />
                        <span className='setting-text'>Setting</span>
                    </div>
                </header>
                <StoriesContainer inputVal={inputValue} />
            </main>
        )
    }
}

export default Header
